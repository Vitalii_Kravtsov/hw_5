import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;


public class TestMyArrayList {

    private final List<String> list = new MyArrayList<>();

    @Test
    public void testConstructor() {

        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(0));
        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(-1));

        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(-1, 1.05));

        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(4, 1));
        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(8, 0.5));

        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(0, 1));
        assertThrows(IllegalArgumentException.class, () -> new MyArrayList<String>(-2, -1.6));

    }

    @Test
    public void testIndexValidation() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Lviv", "Paris", "Madrid", "Paris"));

        assertThrows(IndexOutOfBoundsException.class, () -> this.list.add(-1, "Peking"));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.add(-100, "Peking"));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.add(5, "Peking"));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.add(100, "Peking"));

        this.list.add(4, "Peking");

        assertEquals("Peking", this.list.get(this.list.size() - 1));

        assertThrows(IndexOutOfBoundsException.class, () -> this.list.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.get(-100));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.get(5));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.get(100));

        this.list.addAll(Arrays.asList("Kharkiv", "Dnipro", "Dnipro"));

        assertThrows(IndexOutOfBoundsException.class, () -> this.list.set(-1, "Shanghai"));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.set(-100, "Shanghai"));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.set(8, "Shanghai"));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.set(100, "Shanghai"));

        assertThrows(IndexOutOfBoundsException.class, () -> this.list.addAll(-1, Arrays.asList("London", "Berlin")));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.addAll(-100, Arrays.asList("London", "Berlin")));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.addAll(9, Arrays.asList("London", "Berlin")));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.addAll(100, Arrays.asList("London", "Berlin")));

        this.list.addAll(8, Arrays.asList("London", "Berlin"));

        assertEquals(10, this.list.size());

        assertThrows(IndexOutOfBoundsException.class, () -> this.list.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.remove(-100));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.remove(10));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.remove(100));

        assertThrows(IndexOutOfBoundsException.class, () -> this.list.subList(-1, 2));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.subList(-1, 15));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.subList(0, 11));
        assertThrows(IndexOutOfBoundsException.class, () -> this.list.subList(11, 2));

        assertThrows(IllegalArgumentException.class, () -> this.list.subList(5, 3));

        assertEquals(new MyArrayList<String>(), this.list.subList(3, 3));

    }

    @Test
    public void testSize() {

        this.list.clear();

        assertEquals(0, this.list.size());

        this.list.add("Kyiv");

        assertEquals(1, this.list.size());

        this.list.addAll(Arrays.asList("Kyiv", "London", "Tokyo", "Tokyo"));

        assertEquals(5, this.list.size());

        this.list.removeAll(Arrays.asList("Tokyo", "Berlin"));

        assertEquals(3, this.list.size());

    }

    @Test
    public void testIsEmpty() {

        this.list.clear();

        assertTrue(this.list.isEmpty());

        this.list.add("New-York");

        assertFalse(this.list.isEmpty());

        this.list.remove(0);

        assertTrue(this.list.isEmpty());

    }

    @Test
    public void testContains() {

        this.list.clear();

        assertFalse(this.list.contains("Kyiv"));

        this.list.addAll(Arrays.asList("Kyiv", "Kyiv", "Cherkassy"));

        assertTrue(this.list.contains("Kyiv"));
        assertTrue(this.list.contains("Cherkassy"));

        assertFalse(this.list.contains("Lviv"));

        assertFalse(this.list.contains(5));
        assertFalse(this.list.contains(new StringBuilder("Kyiv")));

    }

    @Test
    public void testIterator() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Kyiv", "Kharkiv", "Kyiv", "Lviv", "Sofia", "Rome", "Rome"));

        Iterator<String> iterator = this.list.iterator();

        assertTrue(iterator.hasNext());

        int i = 0;

        while (iterator.hasNext())
            assertEquals(this.list.get(i++), iterator.next());

        assertFalse(iterator.hasNext());

        i = 0;

        for (String element : this.list)
            assertEquals(this.list.get(i++), element);

    }

    @Test
    public void testToArray() {

        this.list.clear();

        assertArrayEquals(new String[0], this.list.toArray());

        this.list.addAll(Arrays.asList("Kyiv", "Kharkiv", "Kyiv", "Lviv", "London", "New-York"));

        assertArrayEquals(new String[]{"Kyiv", "Kharkiv", "Kyiv", "Lviv", "London", "New-York"}, this.list.toArray());

        this.list.removeAll(Collections.singletonList("Kyiv"));

        assertArrayEquals(new String[]{"Kharkiv", "Lviv", "London", "New-York"}, this.list.toArray());

    }

    @Test
    public void testAdd() {

        this.list.clear();

        assertTrue(this.list.add("Berlin"));

        assertEquals(1, this.list.size());

        assertEquals("Berlin", this.list.get(this.list.size() - 1));

        assertTrue(this.list.add("Sidney"));

        assertEquals(2, this.list.size());

        assertEquals("Sidney", this.list.get(this.list.size() - 1));

    }

    @Test
    public void testRemove() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Kyiv", "Kharkiv", "Kyiv", "Lviv", "Sofia", "Rome", "Rome"));

        assertEquals(7, this.list.size());

        assertTrue(this.list.remove("Sofia"));

        assertEquals(6, this.list.size());

        assertTrue(this.list.remove("Kyiv"));

        assertEquals(5, this.list.size());

        assertFalse(this.list.remove("Peking"));

        assertEquals(5, this.list.size());

        assertFalse(this.list.remove(new StringBuilder("Lviv")));

        assertEquals(5, this.list.size());

    }

    @Test
    public void testContainsAll() {

        this.list.clear();

        assertTrue(this.list.containsAll(new ArrayList<String>()));
        assertTrue(this.list.containsAll(new ArrayList<Integer>()));

        assertFalse(this.list.containsAll(Arrays.asList("Tokyo", "Peking")));

        this.list.addAll(Arrays.asList("Kyiv", "Kharkiv", "Kyiv", "Lviv", "Sofia", "Rome", "Rome"));

        assertTrue(this.list.containsAll(Arrays.asList("Kyiv", "Lviv", "Rome")));
        assertTrue(this.list.containsAll(Arrays.asList("Kyiv", "Kyiv", "Kyiv", "Lviv", "Rome")));

        assertFalse(this.list.containsAll(Arrays.asList("Kyiv", "Lviv", "Rome", "Oslo")));

    }

    @Test
    public void testAddAll() {

        this.list.clear();

        assertEquals(0, this.list.size());

        assertTrue(this.list.addAll(Collections.singletonList("Kyiv")));

        assertEquals(1, this.list.size());

        assertTrue(this.list.addAll(Arrays.asList("Lviv", "Chernihiv", "Lviv")));

        assertEquals(4, this.list.size());

        assertFalse(this.list.addAll(new ArrayList<>()));

        assertEquals(4, this.list.size());

        assertEquals("Kyiv", this.list.get(0));
        assertEquals("Lviv", this.list.get(this.list.size() - 1));

    }

    @Test
    public void testAddAllBeforeIndex() {

        this.list.clear();

        assertTrue(this.list.addAll(0, Arrays.asList("Tokyo", "Peking", "Paris", "London")));

        assertArrayEquals(new String[]{"Tokyo", "Peking", "Paris", "London"}, this.list.toArray());

        assertFalse(this.list.addAll(2, new ArrayList<>()));

        assertTrue(this.list.addAll(0, Arrays.asList("Rome", "Berlin", "Rome", "Paris")));

        assertArrayEquals(new String[]{"Rome", "Berlin", "Rome", "Paris", "Tokyo", "Peking", "Paris", "London"}, this.list.toArray());

        assertTrue(this.list.addAll(3, Arrays.asList("Lviv", "Kyiv", "Dnipro")));

        assertArrayEquals(
                new String[]{"Rome", "Berlin", "Rome", "Lviv", "Kyiv", "Dnipro", "Paris", "Tokyo", "Peking", "Paris", "London"},
                this.list.toArray()
        );

        assertTrue(this.list.addAll(this.list.size(), Arrays.asList("Kharkiv", "Kharkiv")));

        assertArrayEquals(
                new String[]{"Rome", "Berlin", "Rome", "Lviv", "Kyiv", "Dnipro", "Paris", "Tokyo", "Peking", "Paris", "London", "Kharkiv", "Kharkiv"},
                this.list.toArray()
        );

    }

    @Test
    public void testRemoveAll() {

        this.list.clear();

        assertFalse(this.list.removeAll(new ArrayList<>()));

        this.list.addAll(Arrays.asList("Rome", "Kyiv", "Peking", "Berlin", "Peking", "Tokyo"));

        assertFalse(this.list.removeAll(new ArrayList<>()));

        assertTrue(this.list.removeAll(Arrays.asList("Kyiv", "Peking", "Lviv")));

        assertArrayEquals(new String[]{"Rome", "Berlin", "Tokyo"}, this.list.toArray());

        assertFalse(this.list.removeAll(Arrays.asList("London", "Madrid")));

        assertArrayEquals(new String[]{"Rome", "Berlin", "Tokyo"}, this.list.toArray());

        assertTrue(this.list.removeAll(Arrays.asList("London", "Rome", "Madrid", "Rome", "Berlin", "Tokyo")));

        assertArrayEquals(new String[0], this.list.toArray());

    }

    @Test
    public void testRetainAll() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Rome", "Kyiv", "Peking", "Berlin", "Peking", "Tokyo", "Peking", "Kyiv", "Sidney"));

        assertFalse(this.list.retainAll(Arrays.asList("Rome", "Kyiv", "Peking", "Berlin", "Tokyo", "Kyiv", "Sidney", "London")));

        assertArrayEquals(
                new String[]{"Rome", "Kyiv", "Peking", "Berlin", "Peking", "Tokyo", "Peking", "Kyiv", "Sidney"},
                this.list.toArray()
        );

        assertTrue(this.list.retainAll(Arrays.asList("Rome", "Kyiv", "Peking", "Paris")));

        assertArrayEquals(
                new String[]{"Rome", "Kyiv", "Peking", "Peking", "Peking", "Kyiv"},
                this.list.toArray()
        );

        assertTrue(this.list.retainAll(new ArrayList<>()));

        assertArrayEquals(new String[0], this.list.toArray());

        assertFalse(this.list.retainAll(Arrays.asList("Rome", "Kyiv", "Peking", "Paris")));

        assertArrayEquals(new String[0], this.list.toArray());

    }

    @Test
    public void testClear() {

        this.list.clear();

        assertTrue(this.list.isEmpty());

        assertEquals(0, this.list.size());

    }

    @Test
    public void testGet() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Rome", "Kyiv", "Peking", "Berlin", "Peking", "Tokyo"));

        assertEquals("Rome", this.list.get(0));
        assertEquals("Tokyo", this.list.get(this.list.size() - 1));
        assertEquals("Peking", this.list.get(2));
        assertEquals("Peking", this.list.get(4));

    }

    @Test
    public void testSet() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Rome", "Kyiv", "Peking", "Berlin", "Peking", "Tokyo"));

        assertEquals("Rome", this.list.set(0, "Paris"));
        assertArrayEquals(new String[]{"Paris", "Kyiv", "Peking", "Berlin", "Peking", "Tokyo"}, this.list.toArray());

        assertEquals("Tokyo", this.list.set(this.list.size() - 1, "Kyiv"));
        assertArrayEquals(new String[]{"Paris", "Kyiv", "Peking", "Berlin", "Peking", "Kyiv"}, this.list.toArray());

        assertEquals("Peking", this.list.set(2, "Oslo"));
        assertArrayEquals(new String[]{"Paris", "Kyiv", "Oslo", "Berlin", "Peking", "Kyiv"}, this.list.toArray());

        assertEquals("Peking", this.list.set(4, "Peking"));
        assertArrayEquals(new String[]{"Paris", "Kyiv", "Oslo", "Berlin", "Peking", "Kyiv"}, this.list.toArray());


    }

    @Test
    public void testAddBeforeIndex() {

        this.list.clear();

        assertArrayEquals(new String[0], this.list.toArray());

        this.list.add(0, "Tokyo");
        assertArrayEquals(new String[]{"Tokyo"}, this.list.toArray());

        this.list.add(0, "Peking");
        assertArrayEquals(new String[]{"Peking", "Tokyo"}, this.list.toArray());

        this.list.add(2, "Peking");
        assertArrayEquals(new String[]{"Peking", "Tokyo", "Peking"}, this.list.toArray());

        this.list.add(1, "Sidney");
        assertArrayEquals(new String[]{"Peking", "Sidney", "Tokyo", "Peking"}, this.list.toArray());

    }

    @Test
    public void testRemoveByIndex() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Peking", "Tokyo", "Peking", "London", "Rome"));

        assertEquals("Peking", this.list.remove(0));
        assertArrayEquals(new String[]{"Tokyo", "Peking", "London", "Rome"}, this.list.toArray());

        assertEquals("Rome", this.list.remove(this.list.size() - 1));
        assertArrayEquals(new String[]{"Tokyo", "Peking", "London"}, this.list.toArray());

        assertEquals("Peking", this.list.remove(1));
        assertArrayEquals(new String[]{"Tokyo", "London"}, this.list.toArray());

        assertEquals("London", this.list.remove(1));
        assertArrayEquals(new String[]{"Tokyo"}, this.list.toArray());

        assertEquals("Tokyo", this.list.remove(0));
        assertArrayEquals(new String[0], this.list.toArray());

    }

    @Test
    public void testIndexOf() {

        this.list.clear();

        this.list.addAll(Arrays.asList("London", "Rome", "Berlin", "Rome", "London", "Paris", "Rome"));

        assertEquals(0, this.list.indexOf("London"));
        assertEquals(1, this.list.indexOf("Rome"));
        assertEquals(5, this.list.indexOf("Paris"));

        assertEquals(-1, this.list.indexOf("Oslo"));

        assertEquals(-1, this.list.indexOf(5));

    }

    @Test
    public void testLastIndexOf() {

        this.list.clear();

        this.list.addAll(Arrays.asList("London", "Rome", "Berlin", "Rome", "London", "Paris", "Rome"));

        assertEquals(4, this.list.lastIndexOf("London"));
        assertEquals(6, this.list.lastIndexOf("Rome"));
        assertEquals(5, this.list.lastIndexOf("Paris"));

        assertEquals(-1, this.list.lastIndexOf("Oslo"));

        assertEquals(-1, this.list.lastIndexOf(10));

    }

    @Test
    public void testSubList() {

        this.list.clear();

        this.list.addAll(Arrays.asList("Kyiv", "Kharkiv", "Kyiv", "Lviv", "Sofia", "Rome", "Rome"));

        assertEquals(this.list, this.list.subList(0, this.list.size()));

        assertEquals(new MyArrayList<>(), this.list.subList(0, 0));
        assertEquals(new MyArrayList<>(), this.list.subList(2, 2));

        List<String> subList = new MyArrayList<>();

        subList.addAll(Arrays.asList("Kyiv", "Kharkiv", "Kyiv", "Lviv"));

        assertEquals(subList, this.list.subList(0, 4));

        subList.clear();

        subList.addAll(Arrays.asList("Kyiv", "Lviv", "Sofia"));

        assertEquals(subList, this.list.subList(2, 5));

    }

    @Test
    public void testToString() {

        this.list.clear();

        assertEquals("MyArrayList: []", this.list.toString());

        this.list.add("+");

        assertEquals("MyArrayList: [+]", this.list.toString());

        this.list.addAll(Arrays.asList("a", "a", "b", "c"));

        assertEquals("MyArrayList: [+, a, a, b, c]", this.list.toString());

    }

    @Test
    public void testEquals() {

        this.list.clear();

        assertEquals(this.list, this.list);

        this.list.addAll(Arrays.asList("a", "a", "b", "c"));

        assertEquals(this.list, this.list);

        assertNotEquals(this.list, new String[]{"a", "a", "b", "c"});
        assertNotEquals(this.list, Arrays.asList("a", "a", "b", "c"));

        List<String> toCompare = new MyArrayList<>();
        toCompare.addAll(Arrays.asList("a", "b", "c"));

        assertNotEquals(toCompare, this.list);

        toCompare.add(1, "a");

        assertEquals(toCompare, this.list);

        toCompare.set(1, "b");

        assertNotEquals(toCompare, this.list);

        assertEquals(new MyArrayList<>(), new MyArrayList<>());

    }

}
