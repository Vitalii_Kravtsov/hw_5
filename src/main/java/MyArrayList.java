import java.util.*;


public class MyArrayList<T> implements List<T> {

    private static final int DEFAULT_CAPACITY = 8;
    private static final double DEFAULT_GROWTH = 1.6;

    private final int capacity;
    private final double growth;

    private Object[] array;

    private int size;

    public MyArrayList(int capacity, double growth) {

        if (capacity < 1)
            throw new IllegalArgumentException("Illegal capacity");

        if (growth <= 1)
            throw new IllegalArgumentException("Illegal growth");

        this.capacity = capacity;
        this.growth = growth;

        this.array = new Object[capacity];

        this.size = 0;

    }

    public MyArrayList(int capacity) {
        this(capacity, MyArrayList.DEFAULT_GROWTH);
    }

    public MyArrayList() {
        this(MyArrayList.DEFAULT_CAPACITY);
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean contains(Object o) {

        for (T e : this)
            if (Objects.equals(e, o))
                return true;

        return false;

    }

    @Override
    public Iterator<T> iterator() {

        return new Iterator<>() {

            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < size();
            }

            @Override
            public T next() {
                return (T) array[index++];
            }

        };

    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(this.array, this.size());
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) Arrays.copyOf(this.array, this.size(), a.getClass());
    }

    @Override
    public boolean add(T t) {

        this.grow(1);

        this.array[this.size++] = t;

        return true;

    }

    @Override
    public boolean remove(Object o) {

        if (!this.contains(o))
            return false;

        for (int i = 0; i < this.size(); i++) {
            if (Objects.equals(this.get(i), o)) {
                this.shift(i);
                break;
            }
        }

        this.reduce();

        return true;

    }

    @Override
    public boolean containsAll(Collection<?> c) {

        for (Object o : c)
            if (!this.contains(o))
                return false;

        return true;

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {

        if (c.isEmpty())
            return false;

        this.grow(c.size());

        for (T t : c)
            this.array[this.size++] = t;

        return true;

    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {

        if (c.isEmpty())
            return false;

        if (index == this.size)
            return this.addAll(c);

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        this.grow(c.size());

        Iterator<? extends T> iterator = c.iterator();

        for (int i = this.size - 1; i >= index; i--)
            this.array[i+c.size()] = this.array[i];

        for (int i = index; i < index + c.size(); i++) {
            this.array[i] = iterator.next();
        }

        this.size += c.size();

        return true;

    }

    @Override
    public boolean removeAll(Collection<?> c) {

        boolean removed = false;

        int i = 0;

        while (i != this.size()) {
            if (c.contains(this.get(i))) {
                this.shift(i);
                removed = true;
            } else i++;
        }

        reduce();

        return removed;

    }

    @Override
    public boolean retainAll(Collection<?> c) {

        boolean removed = false;

        int i = 0;

        while (i != this.size()) {
            if (!c.contains(this.get(i))) {
                this.shift(i);
                removed = true;
            } else i++;
        }

        reduce();

        return removed;

    }

    @Override
    public void clear() {

        this.array = new Object[this.capacity];

        this.size = 0;

    }

    @Override
    public T get(int index) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        return (T) this.array[index];

    }

    @Override
    public T set(int index, T element) {

        T old = this.get(index);

        this.array[index] = element;

        return old;

    }

    @Override
    public void add(int index, T element) {

        if (index == this.size())
            this.add(element);

        else {

            if (!this.validateIndex(index))
                throw new IndexOutOfBoundsException(index);

            this.grow(1);

            for (int i = this.size - 1; i >= index; i--)
                this.array[i + 1] = this.array[i];

            this.array[index] = element;

            this.size++;

        }

    }

    @Override
    public T remove(int index) {

        T old = this.get(index);

        this.shift(index);

        return old;

    }

    @Override
    public int indexOf(Object o) {

        for (int i = 0; i < this.size(); i++)
            if (Objects.equals(this.get(i), o))
                return i;

        return -1;

    }

    @Override
    public int lastIndexOf(Object o) {

        for (int i = this.size() - 1; i >= 0; i--)
            if (Objects.equals(this.get(i), o))
                return i;

        return -1;

    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {

        if (!this.validateIndex(fromIndex))
            throw new IndexOutOfBoundsException(fromIndex);

        if (toIndex != this.size() && !this.validateIndex(toIndex))
            throw new IndexOutOfBoundsException(fromIndex);

        if (fromIndex > toIndex)
            throw new IllegalArgumentException("fromIndex > toIndex");

        List<T> result = new MyArrayList<>();

        for (int i = fromIndex; i < toIndex; i++) {
            result.add(this.get(i));
        }

        return result;

    }

    @Override
    public String toString() {

        if (this.isEmpty())
            return "MyArrayList: []";

        StringBuilder result = new StringBuilder("MyArrayList: [");

        for (int i = 0; i < this.size() - 1; i++)
            result.append(this.array[i]).append(", ");

        result.append(this.array[this.size-1]).append("]");

        return result.toString();

    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (!(o instanceof MyArrayList))
            return false;

        List<?> list = (MyArrayList<?>) o;

        if (this.size() != list.size())
            return false;

        for (int i = 0; i < this.size(); i++)
            if (!Objects.equals(this.get(i), list.get(i)))
                return false;

        return true;

    }

    private void grow(int delta) {

        if (delta > 0 && this.size + delta > this.array.length) {

            int newLength = this.array.length;

            while (newLength < this.size + delta)
                newLength = (int) Math.ceil(newLength * this.growth);

            this.array = Arrays.copyOf(this.array, newLength);

        }

    }

    private void reduce() {

        int newLength = (int) (this.array.length / this.growth);

        if (newLength >= this.size()) {

            while (newLength > this.capacity && (int) (newLength / this.growth) >= this.size())
                newLength = (int) (newLength / this.growth);

            this.array = Arrays.copyOf(this.array, newLength);

        }

    }

    private void shift(int index) {

        for (int i = index; i < this.size() - 1; i++)
            this.array[i] = this.array[i+1];

        this.array[this.size - 1] = null;

        this.size--;

    }

    private boolean validateIndex(int index) {
        return index >= 0 && index < this.size;
    }

}
