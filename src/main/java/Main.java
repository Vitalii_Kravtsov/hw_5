import java.util.Arrays;
import java.util.List;


public class Main {

    public static void main(String[] args) {

        List<String> list = new MyArrayList<>();

        System.out.println(list.isEmpty());

        System.out.printf("%s %s\n", list, list.size());

        list.add("Petya");
        list.add("Vasya");
        list.add("Ivan");

        System.out.printf("%s %s\n", list, list.size());

        System.out.println(list.isEmpty());

        list.clear();

        list.addAll(Arrays.asList("Roma", "Oleksandr", "Ivan", "Ivan", "Sergey", "Andrew", "Ivan", "Andrew", "Mike", "Stepan"));

        System.out.printf("%s %s\n", list, list.size());

        list.remove("Andrew");

        System.out.printf("%s %s\n", list, list.size());

        list.removeAll(Arrays.asList("Ivan", "Petro"));

        System.out.printf("%s %s\n", list, list.size());

        list.remove(5);

        System.out.printf("%s %s\n", list, list.size());

        list.addAll(Arrays.asList("Ivan", "Ivan", "Ivan", "Nastya", "Alina", "Maria", "Maria", "Viktor"));

        System.out.printf("%s %s\n", list, list.size());

        list.retainAll(Arrays.asList("Ivan", "Alina", "Dmytro"));

        System.out.printf("%s %s\n", list, list.size());

    }

}
